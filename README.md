**extraer_dicom_00.py**

*Por Ángel Ramírez Isea (Asociación Cooperativa Simón Rodríguez para el Conocimiento Libre, R.S.)*

Este programa abre un PDF con las adjudicaciones de divisas de una subasta del DICOM, y extrae los datos para que puedan ser útiles (machine-readable).

Se puede modificar fácilmente para generar un archivo .csv. También se pueden parametrizar algunas cosas, pero libero el codigo para que lo haga alguien con más tiempo.

Dependencias: *pdftotext*

Licencia GPLv3.

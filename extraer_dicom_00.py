# -*- coding: utf-8 -*-
###############################################################################
#
# extraer_dicom_00.py
#
# Por Ángel Ramírez Isea (Asociación Cooperativa Simón Rodríguez para el
# Conocimiento Libre, R.S.)
#
# Este programa abre un PDF con las adjudicaciones de divisas de una subasta
# del DICOM, y extrae los datos para que puedan ser útiles (machine-readable).
#
# Se puede modificar fácilmente para generar un archivo .csv. También se pueden
# parametrizar algunas cosas, pero libero el codigo para que lo haga alguien
# con más tiempo.
#
# Dependencias: pdftotext
#
# Licencia GPLv3.
#
###############################################################################
import subprocess

pathn = "/tu/ruta/al/archivo/"  # Por ejemplo /home/user/Descargas
pdf_f = "ADJUDICADOS-SUBASTA-CS-SO-001-17-v2-firmado"  # Parametrizable.
filename =  pathn + pdf_f
count = 0
jump = 18  # Esta es la altura óptima de las líneas para este PDF.
for y in range(0, 545, jump):
    num_pages = 27  # Fácilmente parametrizable. Obvié las adjudicaciones a
    # personas naturales.
    for page in range(1, num_pages + 1):
        extracto = subprocess.call([
            "pdftotext", "-f", str(page), "-l", str(page), "-x", "0", "-y",
            str(y), "-W", "700", "-H", str(jump), filename + ".pdf",
            filename + ".txt"
        ])
        with open(filename + ".txt") as f:
            lineas = f.readlines()
            lineas = [x.strip('\n') for x in lineas]
            # En esta implementación sólo nos interesan la personas jurídicas.
            if len(lineas[0]) > 8 and lineas[0][0] == "J":
                count += 1
                # Agregué un contador mientras obtenia la altura de línea
                # ideal. Se puede eliminar si se va a exportar a csv, o bien
                # sustituirlo por la fecha de la subasta, el nombre del archivo
                # fuente, o algun otro dato de interés.
                print str(count).zfill(3) + " " + lineas[0],
                if len(lineas[2]) > 1:
                    # Usé ljust() para hacer las columnas uniformes, aunque los
                    # acentos requiren mejor manejo. En todo caso, se puede
                    # eliminar el padding y agregar comas la final para hacer
                    # un csv.
                    print lineas[2].ljust(57),
                if len(lineas[4]) > 1:
                    # Por alguna razón el monto y la causa de la adjudicación
                    # están en una misma línea. El monto se puede limpiar con
                    # replace() para quitar primero el punto y luego sustituir
                    # la coma por un punto, y así poder operar matemáticamente
                    # sobre los montos.
                    #
                    # Las causas de adjudicación se pueden agrupar, de manera
                    # que permita aplicar criterios de búsqueda útiles.
                    print lineas[4].split(' ')[0].rjust(20), \
                        " ".join(lineas[4].split(' ')[1:])
    subprocess.call(["rm", "-f", filename + ".txt"])  # Limpieza final.

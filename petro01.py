# -*- coding: utf-8 -*-

import urllib2
from bs4 import BeautifulSoup
from babel.numbers import format_decimal
from PIL import Image, ImageDraw, ImageFont
from datetime import date

URL_PDVSA = 'http://www.pdvsa.com/index.php?lang=es'
URL_EUROP = 'https://www.ecb.europa.eu/rss/fxref-usd.html'
URL_DICOM = 'https://www.dicom.gob.ve/'


def show_line(x, y, message, color, font):
    draw.text((x, y), message, fill=color, font=font)


def to_float(string):
    return float(string.replace('.', '').replace(',', '.'))


image = Image.open('background.png')
draw = ImageDraw.Draw(image)
font = ImageFont.truetype('FreeMono.ttf', size=18)
fontB = ImageFont.truetype('FreeMonoBold.ttf', size=20)
fontS = ImageFont.truetype('FreeMono.ttf', size=12)
color = 'rgb(0, 0, 0)'  # black

(x, y) = (70, 30)
message = "Elaborado: " + str(date.today())
show_line(x, y, message, color, fontB)

(x, y) = (15, 70)
offset = 135

req = urllib2.Request(URL_PDVSA)
response = urllib2.urlopen(req, timeout=15)
sopa = str(BeautifulSoup(response.read(), "lxml"))
respuesta1 = sopa.split('Cesta Venezolana')[0]
petro = respuesta1.split('¥')[1].split('</span>')[0].strip()
message = "CNY/PTR  = " + petro.rjust(12) + " (PDVSA)"
show_line(x, y, message, color, font)
y += 20

req = urllib2.Request(URL_EUROP)
response = urllib2.urlopen(req, timeout=15)
sopa = str(BeautifulSoup(response.read(), "lxml"))
criterio = '<rdf:li rdf:resource="http://www.ecb.europa.eu/stats/exchange/'
criterio += 'eurofxref/html/eurofxref-graph-usd.en.html?date='
respuesta1 = sopa.split(criterio)[1]
parte = respuesta1.split('&amp;rate=')[1]
euro = parte.split('"></rdf:li>')[0]
message = "USD/EUR  = " + format_decimal(round(float(euro), 2)).rjust(12) +\
    " (UE)"
show_line(x, y, message, color, font)
y += 20

req = urllib2.Request(URL_DICOM)
response = urllib2.urlopen(req, timeout=15)
sopa = str(BeautifulSoup(response.read(), "lxml"))
respuesta1 = sopa.split('<section class="moneda moneda-')
for moneda in range(4):
    parte = respuesta1[moneda + 1].split('</section')[0]
    moneda = parte[:3].upper()
    valor = parte.split('<p class="value">')[1].split('</p')[0]
    message = "VEF/" + moneda + "  = " + valor.rjust(12) + " (DICOM)"
    show_line(x, y, message, color, font)
    y += 20
    valors = format_decimal(round(to_float(valor) / 1000.0, 2))
    message = "VES/" + moneda + "  = " + valors.rjust(12) + " (DICOM)"
    show_line(x, y + offset, message, color, font)
    if moneda == "CNY":
        message = "VEF/PTR  = " +\
            format_decimal(
                round(to_float(valor) * to_float(petro), 2),
                locale='es_VE.UTF-8'
            ).rjust(12) +\
            " (Calculado)"
        show_line(x, y, message, color, font)
        y += 20
        message = "VES/PTR  = " +\
            format_decimal(
                round(to_float(valor) * to_float(petro) / 1000.0, 2),
                locale='es_VE.UTF-8'
            ).rjust(12) +\
            " (Calculado)"
        show_line(x, y + offset, message, color, font)
        message = "VEF/mene = " +\
            format_decimal(
                round(to_float(valor) * to_float(petro) / 100000.0, 2),
                locale='es_VE.UTF-8'
            ).rjust(12) +\
            " (Calculado)"
        show_line(x, y, message, color, font)
        y += 20
        message = "VES/mene = " +\
            format_decimal(
                round(to_float(valor) * to_float(petro) / 100000000.0, 2),
                locale='es_VE.UTF-8'
            ).rjust(12) +\
            " (Calculado)"
        show_line(x, y + offset, message, color, font)

dolar = round(to_float(valor) / float(euro), 2)
message =  "VEF/USD  = " +\
    format_decimal(dolar, locale='es_VE.UTF-8').rjust(12) + " (Calculado)"
show_line(x, y, message, color, font)
y += 20
valors = format_decimal(round(dolar / 1000.0, 2), locale='es_VE.UTF-8')
message = "VES/USD  = " + valors.rjust(12) + " (Calculado)"
show_line(x, y + offset, message, color, font)
y += 40

message = u"El Petro: Demoledor contrataque en la guerra económica"
show_line(x + 3, y + offset, message, color, fontS)
y += 15
message = u"¡Movimiento Cooperativista activado!"
show_line(x + 60, y + offset, message, color, fontS)

image.save('cotizacion_petro_' + str(date.today()) + '.png')
